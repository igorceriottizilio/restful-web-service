package com.example.restful;

public class Nos {

    private long nos;

    public long nosParaKmPorHora() {
        return Math.round(this.nos*1.852);
    }

    public long getNos() {
        return nos;
    }

    public void setNos(long nos) {
        this.nos = nos;
    }

    
    
}
