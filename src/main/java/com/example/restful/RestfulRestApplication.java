package com.example.restful;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/api")
public class RestfulRestApplication extends Application {
}
