package com.example.restful;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

/**
 *
 */
@Path("/converter")
@Singleton
public class HelloController {
    
    @POST
    @Path("/quilometroHoraParaMilhasPorHora")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public long converterKmPorHoraParaMilhasPorHora(@FormParam("quilometroPorHora") Long quilometroPorHora) {

        Quilometro quilometro = new Quilometro();
        quilometro.setKmPorHora(quilometroPorHora);
        return quilometro.converterParaMilhasPorHora();

    }

    @POST
    @Path("/nosParaQuilometrosPorHora")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public long converterNosParaKmPorHora(String json) {

        Gson g = new Gson();
        Nos nos = g.fromJson(json, Nos.class);
        return nos.nosParaKmPorHora();    

    }

}
