package com.example.restful;

public class Quilometro {

    private long kmPorHora;

    public long converterParaMilhasPorHora() {
        return Math.round(this.kmPorHora*0.621371);
    }

    public long getKmPorHora() {
        return kmPorHora;
    }

    public void setKmPorHora(Long kmPorHora) {
        this.kmPorHora = kmPorHora;
    }
}
